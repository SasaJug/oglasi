package com.sasaj.oglasi.list;

import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.sasaj.oglasi.addad.events.NewAdEvent;
import com.sasaj.oglasi.domain.CloudinaryImageStorage;
import com.sasaj.oglasi.domain.FirebaseApi;
import com.sasaj.oglasi.domain.FirebaseHelper;
import com.sasaj.oglasi.domain.ImageStorageFinishedListener;
import com.sasaj.oglasi.domain.event.UploadEvent;
import com.sasaj.oglasi.entities.Ad;
import com.sasaj.oglasi.entities.Photo;
import com.sasaj.oglasi.libs.EventBus;
import com.sasaj.oglasi.libs.GreenRobotEventBus;
import com.sasaj.oglasi.list.events.AdsEvent;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by DS on 5/25/2017.
 */

public class ListRepositoryImpl implements ListRepository {

    private static final String TAG = ListRepositoryImpl.class.getSimpleName();

    private final FirebaseHelper helper;
    private final FirebaseApi firebase;
    private final CloudinaryImageStorage imageStorage;
    private ChildEventListener adEventListener;

    public ListRepositoryImpl() {
        helper = FirebaseHelper.getInstance();
        firebase = FirebaseApi.getInstance();
        imageStorage = new CloudinaryImageStorage(GreenRobotEventBus.getInstance());
    }

    @Override
    public void destroyAddListener() {
        adEventListener = null;
    }

    @Override
    public void subscribeForAddUpates() {
        if (adEventListener == null) {
            adEventListener = new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String previousChildKey) {

                    final Ad ad = dataSnapshot.getValue(Ad.class);
                    ad.setId(dataSnapshot.getKey());
                    Log.e(TAG, "onChildAdded: "+ad.getName() + " " +ad.getId());
                    String posterEmail = ad.getPosterEmail();
                    String currentUserEmail = helper.getAuthUserEmail();
                    ad.setMyAd(posterEmail.equals(currentUserEmail));

                    AdsEvent adEvent = new AdsEvent(ad, null);
                    EventBus eventBus = GreenRobotEventBus.getInstance();
                    eventBus.post(adEvent);

                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String previousChildKey) {
                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                }

                @Override
                public void onCancelled(DatabaseError firebaseError) {
                }
            };
            helper.getAdsReference().addChildEventListener(adEventListener);
        }
    }

    @Override
    public void unSubscribeForAddUpates() {
        if (adEventListener != null) {
            helper.getAdsReference().removeEventListener(adEventListener);
        }
    }

    @Override
    public void postAd(Ad ad) {

        ad.setPosterEmail(helper.getAuthUserEmail());
        ad.setPosterName(helper.getUser().getDisplayName());
        ad.setTime(System.currentTimeMillis());
        DatabaseReference chatsReference = helper.getAdsReference();
        chatsReference.push().setValue(ad);

        EventBus eventBus = GreenRobotEventBus.getInstance();
        eventBus.post(new NewAdEvent(ad, null));
    }


    @Override
    public void uploadPhoto(String path) {
        final String newPhotoId = firebase.create();
        final Photo photo = new Photo();
        photo.setId(newPhotoId);
        photo.setEmail(firebase.getAuthEmail());


        post(UploadEvent.UPLOAD_INIT);
        imageStorage.upload(new File(path), photo.getId(), new ImageStorageFinishedListener(){

            @Override
            public void onSuccess() {
                String url = imageStorage.getImageUrl(photo.getId());
                photo.setUrl(url);
//                firebase.update(photo);

                postPhoto(UploadEvent.UPLOAD_COMPLETE,photo);
            }

            @Override
            public void onError(String error) {
                post(UploadEvent.UPLOAD_ERROR, error);
            }
        });
    }

    private void post(int type){
        post(type, null);
    }

    private void post(int type, String error){
        UploadEvent event = new UploadEvent();
        event.setType(type);
        event.setError(error);
        EventBus eventBus = GreenRobotEventBus.getInstance();
        eventBus.post(event);
    }

    private void postPhoto(int type, Photo photo){
        UploadEvent event = new UploadEvent();
        event.setType(type);
        event.setPhoto(photo);
        EventBus eventBus = GreenRobotEventBus.getInstance();
        eventBus.post(event);
    }

    @Override
    public String getCurrentEmail() {
        return helper.getAuthUserEmail();
    }

    @Override
    public void signOff() {
        helper.signOff();
    }

    @Override
    public void changeUserConnectionStatus(boolean online) {
//        helper.changeUserConnectionStatus(online);
    }
}

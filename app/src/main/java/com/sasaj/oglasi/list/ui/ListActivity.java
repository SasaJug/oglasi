package com.sasaj.oglasi.list.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.sasaj.oglasi.R;
import com.sasaj.oglasi.addad.ui.NewAdActivity;
import com.sasaj.oglasi.addad.ui.NewAdActivity_;
import com.sasaj.oglasi.domain.FirebaseHelper;
import com.sasaj.oglasi.entities.Ad;
import com.sasaj.oglasi.list.ListContract;
import com.sasaj.oglasi.list.ListPresenter;
import com.sasaj.oglasi.list.adapter.AdsAdapter;
import com.sasaj.oglasi.login.MainActivity_;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;


/**
 * Created by DS on 5/24/2017.
 */

@EActivity(R.layout.activity_ad_list)
public class ListActivity extends AppCompatActivity implements ListContract.View {

    private static final String TAG = ListActivity.class.getSimpleName();

    @ViewById(R.id.toolbar)
    android.support.v7.widget.Toolbar toolbar;

    @ViewById(R.id.recyclerView)
    android.support.v7.widget.RecyclerView recyclerView;

    @ViewById(R.id.fab)
    android.support.design.widget.FloatingActionButton fab;

    @ViewById(R.id.progress_bar)
    ProgressBar progressBar;

    @Bean(ListPresenter.class)
    ListPresenter presenter;


    private OnAdClickListener clickllListener = new OnAdClickListener() {
        @Override
        public void onAdClick(Ad ad) {

        }
    };

    private AdsAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter.setView(this);
        presenter.onCreate();
    }

    @Override
    public void onResume() {
        Log.e(TAG, "onResume: " );
        presenter.onResume();
        super.onResume();
    }

    @Override
    public void onPause() {
        Log.e(TAG, "onPause: " );
        presenter.onPause();
        super.onPause();

    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void setAd(Ad ad) {
        if(!ad.getPosterEmail().equals(FirebaseHelper.getInstance().getAuthUserEmail()))
        adapter.setItems(ad);
    }

    @Override
    public void onError(String error) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_myads) {
            MyAdsActivity_.intent(this).start();
        }
        if (id == R.id.action_logout) {
            presenter.signOff();
            MainActivity_.intent(this).flags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK).start();
        }

        return super.onOptionsItemSelected(item);
    }

    @AfterViews
    void setupViews(){

        setSupportActionBar(toolbar);

        adapter = new AdsAdapter(new ArrayList<Ad>(0), clickllListener);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 1));
        recyclerView.setAdapter(adapter);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NewAdActivity_.intent(ListActivity.this).start();
            }
        });
    }


}

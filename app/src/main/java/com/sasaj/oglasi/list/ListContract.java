package com.sasaj.oglasi.list;

import com.sasaj.oglasi.entities.Ad;
import com.sasaj.oglasi.list.events.AdsEvent;

import java.util.List;

/**
 * Created by DS on 5/24/2017.
 */

public interface ListContract {

    interface View {
        void showProgress();

        void hideProgress();

        void setAd(Ad ad);

        void onError(String error);
    }

    interface UserActionsListener {

        void setView(ListContract.View view);

        void onCreate();

        void onResume();

        void onPause();

        void onDestroy();

        void signOff();

        void onEventMainThread(AdsEvent event);
    }
}

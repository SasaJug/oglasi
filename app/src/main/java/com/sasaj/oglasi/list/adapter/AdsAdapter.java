package com.sasaj.oglasi.list.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sasaj.oglasi.R;
import com.sasaj.oglasi.entities.Ad;
import com.sasaj.oglasi.list.ui.OnAdClickListener;
import com.sasaj.oglasi.utilities.Utilities;

import org.androidannotations.annotations.EBean;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by DS on 5/25/2017.
 */

public class AdsAdapter extends RecyclerView.Adapter<AdsAdapter.ViewHolder> {
    private List<Ad> dataset;
    private OnAdClickListener clickListener;

    public AdsAdapter(List<Ad> dataset, OnAdClickListener clickListener) {
        this.dataset = dataset;
        this.clickListener = clickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ads_content, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Ad ad = dataset.get(position);
        holder.setOnClickListener(ad, clickListener);
        holder.title.setText(ad.getName());
        holder.description.setText(ad.getDescription());
        holder.poster.setText(ad.getPosterName());
        holder.date.setText(Utilities.getDate(ad.getTime(),"dd-MM-yyyy"));
    }

    public void setItems(Ad newItem) {
        dataset.add(newItem);
        Collections.sort(dataset);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.description)
        TextView description;
        @BindView(R.id.poster_name)
        TextView poster;
        @BindView(R.id.date)
        TextView date;
        private View view;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.view = itemView;
        }

        public void setOnClickListener(final Ad ad, final OnAdClickListener listener) {
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onAdClick(ad);
                }
            });
        }
    }
}

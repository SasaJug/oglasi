package com.sasaj.oglasi.list;

import com.sasaj.oglasi.entities.Ad;
import com.sasaj.oglasi.libs.EventBus;
import com.sasaj.oglasi.libs.GreenRobotEventBus;
import com.sasaj.oglasi.list.events.AdsEvent;

import org.androidannotations.annotations.EBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DS on 5/25/2017.
 */

@EBean
public class ListInteractorImpl implements ListInteractor {

    private final ListRepositoryImpl repository;

    public ListInteractorImpl() {
        this.repository = new ListRepositoryImpl();
    }

    @Override
    public void destroyAddListener() {
        repository.destroyAddListener();
    }

    @Override
    public void subscribeForAddUpates() {
        repository.subscribeForAddUpates();
    }

    @Override
    public void unSubscribeForAddUpates() {
        repository.unSubscribeForAddUpates();
    }

    @Override
    public void postAd(Ad ad) {
        repository.postAd(ad);
    }
}

package com.sasaj.oglasi.list;

import org.androidannotations.annotations.EBean;

/**
 * Created by DS on 5/26/2017.
 */

@EBean
public class ListSessionInteractorImpl implements ListSessionInteractor {
    ListRepository listRepository;

    public ListSessionInteractorImpl() {
        this.listRepository = new ListRepositoryImpl();
    }

    @Override
    public void signOff() {
        listRepository.signOff();
    }

    @Override
    public String getCurrentUserEmail() {
        return listRepository.getCurrentEmail();
    }

    @Override
    public void changeConnectionStatus(boolean online) {
        listRepository.changeUserConnectionStatus(online);
    }
}

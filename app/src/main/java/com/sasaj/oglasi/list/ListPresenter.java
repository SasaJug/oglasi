package com.sasaj.oglasi.list;

import com.sasaj.oglasi.entities.Ad;
import com.sasaj.oglasi.entities.User;
import com.sasaj.oglasi.libs.EventBus;
import com.sasaj.oglasi.libs.GreenRobotEventBus;
import com.sasaj.oglasi.list.events.AdsEvent;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by DS on 5/25/2017.
 */

@EBean
public class ListPresenter implements ListContract.UserActionsListener {

    @Bean(ListSessionInteractorImpl.class)
    ListSessionInteractor listSessionInteractor;

    @Bean(ListInteractorImpl.class)
    ListInteractor listInteractor;

    @Bean(GreenRobotEventBus.class)
    EventBus eventBus;

    private ListContract.View view;

    @Override
    public void setView(ListContract.View view) {
        this.view = view;
    }

    @Override
    public void onCreate() {
        eventBus.register(this);
    }

    @Override
    public void onResume() {
        listInteractor.subscribeForAddUpates();
    }

    @Override
    public void onPause() {
        listInteractor.unSubscribeForAddUpates();
    }

    @Override
    public void onDestroy() {
        eventBus.unregister(this);
        listInteractor.destroyAddListener();
        view = null;
    }

    @Override
    public void signOff() {
        listSessionInteractor.changeConnectionStatus(User.OFFLINE);
        listInteractor.destroyAddListener();
        listInteractor.unSubscribeForAddUpates();
        listSessionInteractor.signOff();
    }

    @Override
    @Subscribe
    public void onEventMainThread(AdsEvent event) {
        String errorMsg = event.getError();
        if (view != null) {
            view.hideProgress();
            if (errorMsg != null) {
                view.onError(errorMsg);
            } else {
                view.setAd(event.getAd());
            }
        }
    }
}

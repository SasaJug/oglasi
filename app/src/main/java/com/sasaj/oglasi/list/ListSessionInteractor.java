package com.sasaj.oglasi.list;

/**
 * Created by DS on 5/26/2017.
 */

public interface ListSessionInteractor {
    void signOff();
    String getCurrentUserEmail();
    void changeConnectionStatus(boolean online);
}

package com.sasaj.oglasi.list;

import com.sasaj.oglasi.entities.Ad;

/**
 * Created by DS on 5/25/2017.
 */

public interface ListInteractor {
    void destroyAddListener();
    void subscribeForAddUpates();
    void unSubscribeForAddUpates();
    void postAd(Ad ad);
}

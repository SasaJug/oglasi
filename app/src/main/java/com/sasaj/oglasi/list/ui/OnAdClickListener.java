package com.sasaj.oglasi.list.ui;

import com.sasaj.oglasi.entities.Ad;

/**
 * Created by DS on 5/26/2017.
 */

public interface OnAdClickListener {
    void onAdClick(Ad ad);
}

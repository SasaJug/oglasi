package com.sasaj.oglasi.list.events;

import com.sasaj.oglasi.entities.Ad;

import java.util.List;

/**
 * Created by DS on 5/25/2017.
 */

public class AdsEvent {
    private Ad ad;
    private String error;

    public AdsEvent(Ad ad, String error) {
        this.ad = ad;
        this.error = error;
    }

    public Ad getAd() {
        return ad;
    }

    public void setAd(Ad ad) {
        this.ad = ad;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}

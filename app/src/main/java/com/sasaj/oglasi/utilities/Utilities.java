package com.sasaj.oglasi.utilities;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by DS on 5/25/2017.
 */

public class Utilities {
//    public static void hideKeyboard(View view) {
//        ((InputMethodManager) AVApplication.getContext()
//                .getSystemService(Context.INPUT_METHOD_SERVICE))
//                .hideSoftInputFromWindow(view.getWindowToken(), 0);
//    }
//
//    public static void showKeyboard(View view) {
//        ((InputMethodManager) AVApplication.getContext()
//                .getSystemService(Context.INPUT_METHOD_SERVICE))
//                .toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
//    }

    public static String getDate(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }
}


package com.sasaj.oglasi.addad.events;

import com.sasaj.oglasi.entities.Ad;

/**
 * Created by DS on 5/25/2017.
 */

public class NewAdEvent {
    private Ad ad;
    private String error;

    public NewAdEvent(Ad ad, String error) {
        this.ad = ad;
        this.error = error;
    }

    public Ad getAd() {
        return ad;
    }

    public void setAd(Ad ad) {
        this.ad = ad;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}

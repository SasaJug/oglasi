package com.sasaj.oglasi.addad;

import android.util.Log;

import com.sasaj.oglasi.addad.events.NewAdEvent;
import com.sasaj.oglasi.domain.event.UploadEvent;
import com.sasaj.oglasi.entities.Ad;
import com.sasaj.oglasi.entities.Photo;
import com.sasaj.oglasi.libs.EventBus;
import com.sasaj.oglasi.libs.GreenRobotEventBus;
import com.sasaj.oglasi.list.ListInteractor;
import com.sasaj.oglasi.list.ListInteractorImpl;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Created by DS on 5/25/2017.
 */

@EBean
public class NewAdPresenter implements NewAdContract.UserActionsListener {

    private static final String TAG = NewAdPresenter.class.getSimpleName();

    private NewAdContract.View view;

    @Bean(NewAdInteractorImpl.class)
    NewAdInteractor newAdInteractor;

    @Bean(GreenRobotEventBus.class)
    EventBus eventBus;

    private Photo photo;

    @Override
    public void setView(NewAdContract.View view) {
        this.view = view;
    }

    @Override
    public void onCreate() {
        eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        eventBus.unregister(this);
        view = null;
    }

    @Override
    public void uploadPhoto(String path) {
        newAdInteractor.uploadPhoto(path);
    }

    @Override
    public void postAd(Ad ad) {
        if (view != null) {
            view.showProgress();
        }
        newAdInteractor.postAd(ad);
    }

    @Override
    @Subscribe
    public void onConfirm(NewAdEvent event) {
        Log.e(TAG, "onConfirm: ");
        if (view != null) {
            view.hideProgress();
            if (event.getError() == null) {
                view.confirm();
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPhotoUploaded(UploadEvent event) {
        Log.e(TAG, "onConfirm: ");
        this.photo = event.getPhoto();
        if (view != null && event.getPhoto() != null) {
            view.hideProgress();
            view.setPhoto(photo);
        }
    }
}

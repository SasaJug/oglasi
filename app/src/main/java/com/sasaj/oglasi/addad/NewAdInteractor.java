package com.sasaj.oglasi.addad;

import com.sasaj.oglasi.entities.Ad;

/**
 * Created by DS on 5/25/2017.
 */

public interface NewAdInteractor {
   void postAd(Ad ad);
   void uploadPhoto(String path);
}

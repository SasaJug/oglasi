package com.sasaj.oglasi.addad;

import com.sasaj.oglasi.entities.Ad;
import com.sasaj.oglasi.list.ListRepository;
import com.sasaj.oglasi.list.ListRepositoryImpl;

import org.androidannotations.annotations.EBean;

/**
 * Created by DS on 5/25/2017.
 */

@EBean
public class NewAdInteractorImpl implements NewAdInteractor {

    ListRepository repository;

    public NewAdInteractorImpl() {
        this.repository = new ListRepositoryImpl();
    }

    @Override
    public void postAd(Ad ad) {
        repository.postAd(ad);
    }

    @Override
    public void uploadPhoto(String path) {
        repository.uploadPhoto(path);
    }
}

package com.sasaj.oglasi.addad;

import com.sasaj.oglasi.addad.events.NewAdEvent;
import com.sasaj.oglasi.entities.Ad;
import com.sasaj.oglasi.entities.Photo;

/**
 * Created by DS on 5/25/2017.
 */

public interface NewAdContract {

    interface View {
        void showProgress();
        void hideProgress();
        void confirm();
        void onError(String error);
        void setPhoto(Photo photo);
    }

    interface UserActionsListener{
        void setView(NewAdContract.View view);
        void onCreate();
        void onDestroy();
        void uploadPhoto(String path);
        void postAd(Ad ad);
        void onConfirm(NewAdEvent event);
    }

}

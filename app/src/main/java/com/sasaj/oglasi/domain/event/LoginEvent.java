package com.sasaj.oglasi.domain.event;

/**
 * Created by DS on 5/24/2017.
 */

public class LoginEvent {
    boolean loggedIn;

    public LoginEvent(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }
}

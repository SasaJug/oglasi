package com.sasaj.oglasi.domain;

 public interface ImageStorageFinishedListener {
    void onSuccess();
    void onError(String error);
}

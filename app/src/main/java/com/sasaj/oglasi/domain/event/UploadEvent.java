package com.sasaj.oglasi.domain.event;

import com.sasaj.oglasi.entities.Photo;

/**
 * Created by DS on 5/27/2017.
 */

public class UploadEvent {
    private int type;
    private String error;
    private Photo photo;
    public final static int UPLOAD_INIT = 0;
    public final static int UPLOAD_COMPLETE = 1;
    public final static int UPLOAD_ERROR = 2;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Photo getPhoto() {
        return photo;
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}

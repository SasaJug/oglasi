package com.sasaj.oglasi.domain;

import android.support.annotation.NonNull;
import android.util.Log;

import com.firebase.ui.auth.AuthUI;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sasaj.oglasi.domain.event.LoginEvent;
import com.sasaj.oglasi.entities.Photo;
import com.sasaj.oglasi.entities.User;
import com.sasaj.oglasi.libs.EventBus;
import com.sasaj.oglasi.libs.GreenRobotEventBus;


import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by DS on 5/24/2017.
 */

public class FirebaseHelper {
    private static final String TAG = FirebaseHelper.class.getSimpleName();
    private final static String SEPARATOR = "___";
    private final static String ADS_PATH = "ads";
    private final static String USERS_PATH = "users";

    private final DatabaseReference dataReference;
    private FirebaseAuth firebaseAuth;
    FirebaseUser user;

    private FirebaseAuth.AuthStateListener authStateListener = new FirebaseAuth.AuthStateListener() {
        @Override
        public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
            user = firebaseAuth.getCurrentUser();
            Log.e(TAG, "onAuthStateChanged: user - " + (user != null));
            if (user != null) {
                userLoggedIn();
            } else {
                userLoggedOut();
            }
        }
    };


    private static class SingletonHolder {
        private static final FirebaseHelper INSTANCE = new FirebaseHelper();
    }

    public static FirebaseHelper getInstance() {
        return SingletonHolder.INSTANCE;
    }


    public FirebaseHelper() {
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseAuth.addAuthStateListener(authStateListener);
        dataReference = FirebaseDatabase.getInstance().getReference();
    }

    private void userLoggedIn() {
        Log.e(TAG, "userLoggedIn: " + (user != null));
        EventBus eventBus = GreenRobotEventBus.getInstance();
        eventBus.post(new LoginEvent(true));
    }

    private void userLoggedOut() {
        Log.e(TAG, "userLoggedOut: " + (user != null));
        EventBus eventBus = GreenRobotEventBus.getInstance();
        eventBus.post(new LoginEvent(false));
    }

    public FirebaseUser getUser() {
        Log.e(TAG, "getUser: " + (user != null));
        if (user != null)
            return user;
        else
            return null;
    }

    public void setUser(FirebaseUser user) {
        this.user = user;
    }

    public DatabaseReference getDataReference() {
        return dataReference;
    }

    public DatabaseReference getUserReference(String email) {
        DatabaseReference userReference = null;
        if (email != null) {
            String emailKey = email.replace(".", "_");
            userReference = dataReference.getRoot().child(USERS_PATH).child(emailKey);
        }
        return userReference;
    }

    public DatabaseReference getMyUserReference() {
        return getUserReference(getAuthUserEmail());
    }

    public String getAuthUserEmail() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        String email = null;
        if (user != null) {
            email = user.getEmail();
        }
        return email;
    }


    public DatabaseReference getAdsReference() {
        return dataReference.getRoot().child(ADS_PATH);
    }


    public void signOff() {
        FirebaseAuth.getInstance().signOut();
    }

}
package com.sasaj.oglasi.domain;

import java.io.File;

/**
 * Created by DS on 5/27/2017.
 */

public interface ImageStorage {
    String getImageUrl(String id);
    void upload(File file, String id, ImageStorageFinishedListener listener);
}

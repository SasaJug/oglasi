package com.sasaj.oglasi.libs;

/**
 * Created by DS on 4/30/2017.
 */

public interface EventBus {
    void register(Object subscriber);
    void unregister(Object subscriber);
    void post(Object event);

}

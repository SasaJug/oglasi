package com.sasaj.oglasi.login;

import com.sasaj.oglasi.domain.FirebaseHelper;
import com.sasaj.oglasi.domain.event.LoginEvent;
import com.sasaj.oglasi.libs.EventBus;
import com.sasaj.oglasi.libs.GreenRobotEventBus;

import org.androidannotations.annotations.EBean;

/**
 * Created by DS on 5/24/2017.
 */

@EBean
public class LoginInteractorImpl implements LoginInteractor {

    private final FirebaseHelper helper;

    public LoginInteractorImpl() {
        helper = FirebaseHelper.getInstance();
    }

    @Override
    public void checkLoggedIn() {
        if(helper.getUser() == null){
            EventBus eventBus = GreenRobotEventBus.getInstance();
            eventBus.post(new LoginEvent(false));
        }else{
            EventBus eventBus = GreenRobotEventBus.getInstance();
            eventBus.post(new LoginEvent(true));
        }
    }
}

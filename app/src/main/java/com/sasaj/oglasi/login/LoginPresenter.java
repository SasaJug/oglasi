package com.sasaj.oglasi.login;

import com.sasaj.oglasi.domain.event.LoginEvent;
import com.sasaj.oglasi.libs.EventBus;
import com.sasaj.oglasi.libs.GreenRobotEventBus;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by DS on 5/24/2017.
 */

@EBean
public class LoginPresenter implements LoginContract.UserActionsListener {

    private LoginContract.View loginView;

    @Bean(GreenRobotEventBus.class)
    EventBus eventBus;

    @Bean(LoginInteractorImpl.class)
    LoginInteractor loginInteractor;

    public void setLoginView(LoginContract.View loginView) {
        this.loginView = loginView;
    }

    @Override
    public void onCreate() {
        eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        loginView = null;
        eventBus.unregister(this);
    }

    @Override
    public void checkForAuthenticatedUser() {
        loginInteractor.checkLoggedIn();
    }

    @Override
    @Subscribe
    public void onLoginEvent(LoginEvent event) {
        if (event.isLoggedIn())
            loginView.goToMainScreen();
        else
            loginView.startLoginFlow();
    }


}

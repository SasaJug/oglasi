package com.sasaj.oglasi.login;

import com.sasaj.oglasi.domain.event.LoginEvent;

/**
 * Created by DS on 5/24/2017.
 */

public interface LoginContract {

    interface View {
        void showProgress();

        void hideProgress();

        void goToMainScreen();

        void startLoginFlow();
    }

    interface UserActionsListener {

        void setLoginView(LoginContract.View view);

        void onCreate();

        void onDestroy();

        void checkForAuthenticatedUser();

        void onLoginEvent(LoginEvent event);

    }
}

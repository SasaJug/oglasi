package com.sasaj.oglasi;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.firebase.client.Firebase;
import com.sasaj.oglasi.domain.event.LoginEvent;
import com.sasaj.oglasi.libs.EventBus;
import com.sasaj.oglasi.libs.GreenRobotEventBus;
import com.sasaj.oglasi.login.LoginContract;
import com.sasaj.oglasi.login.MainActivity;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EApplication;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by DS on 5/26/2017.
 */

@EApplication
public class AdsApplication extends Application {
    private static final String TAG = AdsApplication.class.getSimpleName();
    private static Context appContext;

    @Bean(GreenRobotEventBus.class)
    EventBus eventBus;

    @Override
    public void onCreate() {
        super.onCreate();
//        this.eventBus = GreenRobotEventBus.getInstance();
        appContext = getApplicationContext();
        Firebase.setAndroidContext(getApplicationContext());
    }

    @Subscribe
    public void logout(LoginEvent event) {
        Log.e(TAG, "logout: " + event.isLoggedIn());
        if (!event.isLoggedIn()) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_NEW_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    public static Context getContext() {
        return appContext;
    }
}

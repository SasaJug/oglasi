package com.sasaj.oglasi.entities;

/**
 * Created by DS on 5/24/2017.
 */

public class Ad implements Comparable{
    private String id;
    private String name;
    private String description;
    private String posterEmail;
    private String posterName;
    private String url;
    private long time;
    private boolean myAd;

    public Ad() {
    }

    public Ad(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPosterEmail() {
        return posterEmail;
    }

    public void setPosterEmail(String posterEmail) {
        this.posterEmail = posterEmail;
    }

    public String getPosterName() {
        return posterName;
    }

    public void setPosterName(String posterName) {
        this.posterName = posterName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isMyAd() {
        return myAd;
    }

    public void setMyAd(boolean myAd) {
        this.myAd = myAd;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    @Override
    public int compareTo(Object o) {
        return Long.compare(((Ad)o).getTime(), this.time);
    }
}
